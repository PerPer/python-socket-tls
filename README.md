The project is a Pyhton TLS program where the programme openssl is used
to produce a self signed certificate and private key on a server.
The project contains both a simple server and a simple client.
Server and client exchange keys and check certificates. The private key
has to be stored on the server. The certificate must recide on both
the server and on the client for authentication.